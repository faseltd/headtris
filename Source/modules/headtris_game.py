#
# headtris_game.py
#
import tetromino
import time
from random import randint

DEF_HIGH_SCORES_FILE_NAME = "python-tetris.scores"

DEF_BOARD_WIDTH = 10
DEF_BOARD_HEIGHT = 20
BOARD_BLOCK_NULL = 255

# Number of milliseconds between block advances.
DEF_DROP_DELAY = 750
DEF_USER_MOVE_DELAY = 75

DEF_LINES_PER_LEVEL_ADVANCE = 10
DEF_POINTS_PER_SOFT_DROP = 1

class Game:
    score = 0
    points_per_soft_drop = DEF_POINTS_PER_SOFT_DROP
    previous_drop_points = 0
    board_width = 0
    board_height = 0
    is_running = False
    is_paused = False
    tetromino_templates = None

    board = None
    active_tetromino = None
    active_tetromino_index = 5

    next_tetromino_index = 0
    next_tetromino_instance_index = 0

    # Timing.
    previous_time_ms = 0
    previous_drop_time_ms = 0

    changed_objects = None

    # User movement control.
    user_move_delta_x = 0
    user_move_delta_y = 0
    previous_move_time_ms = 0
    user_move_delay = DEF_USER_MOVE_DELAY

    # Options.
    new_tetrominoes_appear_centred = True

    # Level advancement control.
    level = 0
    lines_zapped_per_level = {}
    lines_per_level_advance = DEF_LINES_PER_LEVEL_ADVANCE
    base_drop_delay = DEF_DROP_DELAY
    drop_delay = DEF_DROP_DELAY
    
    def __init__( self ):
        print( "Initialising game." )
        self.set_board_size( DEF_BOARD_WIDTH, DEF_BOARD_HEIGHT )
        self.reset()


    def reset( self ):
        self.is_running = False
        self.is_paused = False
        self.lines_per_level_advance = DEF_LINES_PER_LEVEL_ADVANCE
        self.lines_zapped_per_level = {}
        self.previous_time_ms = 0
        self.previous_drop_time_ms = 0
        self.drop_delay = DEF_DROP_DELAY
        self.base_drop_delay = DEF_DROP_DELAY
        self.score = 0
        self.points_per_soft_drop = DEF_POINTS_PER_SOFT_DROP
        self.set_level( 1 )
        self.previous_drop_points = 0
        self.clear_board()
        self.randomise_next_tetromino()
        self.set_next_active_tetromino()
        self.changed_objects = ChangedObjects()

        
    def start( self ):
        if not self.is_running:
            print( "Starting game" )
            self.is_running = True
            self.is_paused = False

        
    def pause( self ):
        if self.is_running and not self.is_paused:
            self.previous_time_ms = 0
            print( "Pausing game." )
            self.is_paused = True


    def resume( self ):
        if self.is_running and self.is_paused:
            print( "Resuming game." )
            self.previous_time_ms = int( round( time.time() * 1000 ) )
            self.is_paused = False


    def stop( self ):
        if self.is_running:
            print( "Stopping game." )
            self.is_running = False
            self.is_paused = False


    def set_next_active_tetromino( self ):
        if self.tetromino_templates is None:
            self.active_tetromino_index = 0
            self.active_tetromino = None
        else:
            # Set the next tetromino to be the active tetromino and generate a new
            # random next tetromino.
            self.active_tetromino_index = self.next_tetromino_index
            self.active_tetromino = self.tetromino_templates[self.active_tetromino_index]
            self.active_tetromino.current_instance = self.next_tetromino_instance_index

            # Locate the new tetromino, either centrally or at a random left
            # position.
            if self.new_tetrominoes_appear_centred:
                self.active_tetromino.left = int( ( self.board_width - self.active_tetromino.edge_length ) / 2 )
            else:
                self.active_tetromino.left = randint( 0, self.board_width - self.active_tetromino.edge_length - 1 )

            self.active_tetromino.top = 0 - self.active_tetromino.edge_length

        # Randomise the next tetromino.
        self.randomise_next_tetromino()


    def randomise_next_tetromino( self ):
        if self.tetromino_templates is not None:
            self.next_tetromino_index = randint( 0, len( self.tetromino_templates ) - 1 )
            self.next_tetromino_instance_index = randint( 0, self.tetromino_templates[self.next_tetromino_index].instance_count - 1 )
            #print( "Next: ", self.next_tetromino_index, ":", self.next_tetromino_instance_index )
        else:
            self.next_tetromino_index = 0
            self.next_tetromino_instance_index = 0

        # Testing.
        #self.next_tetromino_index = 4
        #self.next_tetromino_instance_index = 0

            
    def set_board_size( self, width, height ):
        if ( int( width ) <= 0 ) or ( int( height ) <= 0 ):
            raise Exception( "Board width and height bust be greater than zero." )

        self.board_width = int( width )
        self.board_height = int( height )
        
        #print( "Creating ", self.board_width, "x", self.board_height, " board." )
        self.board = []
        for y in range( self.board_height ):
            row = []
            for x in range( self.board_width ):
                row.append( BOARD_BLOCK_NULL )

            self.board.append( row )


    # Utility method. Consider removing.
    def clear_board( self ):
        self.set_board_size( self.board_width, self.board_height )

        
    def get_is_running( self ):
        return self.is_running
    

    def load_tetrominoes( self, file_name ):
        self.tetromino_templates = []
        piece_size = 0
        with open( file_name, "r" ) as file_stream:
            print( "Loading tetrominoes: ", file_name )
            piece_size = int( file_stream.readline() )

            # Read each group of 'piece_size' lines.
            row_index = 0
            instance_index = 0
            row_string = ""
            row_strings = []
            while (row_index == 0 ) or len( row_string ) > 0:
                row_string = file_stream.readline().strip()

                if len( row_string ) > 0:
                    #print( row_string )
                    row_strings.append( row_string )
                    row_index += 1
                
                #if ( row_index % piece_size ) == 0:
                if len( row_strings ) == piece_size:
                    self.tetromino_templates.append( tetromino.Tetromino( piece_size, row_strings ) )
                    row_strings = []
                    

            file_stream.close()

        #print( "Piece templates = ", len( self.tetromino_templates ) )

        self.randomise_next_tetromino()


    def is_tetromino_location_ok( self, tetromino_index, instance_index, test_x, test_y ):
        #
        # Given a tetromino index, instance (rotation) and an x/y pair, check
        # to see if the piece is allowed in the suggested location by comparing
        # it against the board matrix.
        is_ok = True
        is_checked = False
        width = self.tetromino_templates[tetromino_index].edge_length
        height = width

        # Check to see if we are overlapping any blocks that are already on the
        # board.
        block_x = 0
        block_y = 0
        while is_ok and not is_checked:
            board_x = test_x + block_x
            board_y = test_y + block_y
            
            # Is the part of the tetromino that we're checking an "on block" or
            # an "off block"?
            test_block = self.tetromino_templates[tetromino_index].instances[instance_index][block_y][block_x:block_x+1]
            if test_block == tetromino.BLOCK_ON_CHAR:
                if board_x < 0:
                    is_ok = False
                elif board_x >= self.board_width:
                    is_ok = False
                elif board_y >= self.board_height:
                    is_ok = False
                elif board_y < 0:
                    # This is OK, as no blocks can be up here, but we need to
                    # trap this condition for range checking on the board
                    # array.
                    pass
                elif self.board[ board_y ][ board_x ] != BOARD_BLOCK_NULL:
                    is_ok = False
                else:
                    pass
            
            # Move to the next test co-ordinates.
            if is_ok:
                block_x += 1
                if block_x >= width:
                    block_x = 0
                    block_y += 1
                    if block_y >= height:
                        is_checked = True

        return is_ok

                        
    def add_active_tetromino_to_board( self ):
        # We don't do a check for the location being OK here. We just overwrite
        # whatever is on the board. If we hit existing rubble when trying to
        # add the active piece then it's game over.
        game_over = False
        if self.is_running and not self.is_paused:
            width = self.tetromino_templates[self.active_tetromino_index].edge_length
            height = width

            piece_x = self.active_tetromino.left
            piece_y = self.active_tetromino.top
            for block_y in range( 0, height ):
                board_y = piece_y + block_y
                for block_x in range( 0, width ):
                    board_x = piece_x + block_x
                    block_char = self.tetromino_templates[self.active_tetromino_index].instances[self.active_tetromino.current_instance][block_y][block_x:block_x+1]
                    if block_char == tetromino.BLOCK_ON_CHAR:
                        if ( board_x >= 0 ) and \
                                ( board_x < self.board_width ) and \
                                ( board_y >= 0 ) and \
                                ( board_y < self.board_height ):

                            self.board[ board_y ][ board_x ] = self.active_tetromino_index
                        else:
                            # We are trying to apply a piece off the top of the
                            # board. Game over.
                            game_over = True
                    else:
                        pass

            if game_over:
                print( "Game over!" )
                self.stop()
            else:
                pass
        else:
            pass


    def process_completed_rows( self, changed_objects ):
        # We want the zapped row indices in reverse order (highest first).
        board_y = self.board_height - 1
        zapped_row_count = 0
        this_drop_points = 0
        while board_y >= 0:
            board_x = 0
            row_is_complete = True
            while ( board_x < self.board_width ) and row_is_complete:
                if self.board[ board_y ][ board_x ] == BOARD_BLOCK_NULL:
                    row_is_complete = False
                else:
                    board_x += 1

            if row_is_complete:
                changed_objects.zapped_row_indices.append( board_y )
                changed_objects.board = True

            board_y -= 1

        # Points for this process check are calculated as:
        # two to the power of ( the number of rows completed - 1 )
        # e.g.
        # 1 row completed = 100 points
        # 2 rows completed = 200 points
        # 3 rows completed = 400 points
        # 4 rows completed = 800 points
        #
        # If points were made on the previous drop then the previous drop
        # points count again on this drop too.
        if len( changed_objects.zapped_row_indices ) > 0:
            for zapped_index in changed_objects.zapped_row_indices:
                #print( "Zap: ", zapped_index )
                self.board.pop( zapped_index )
                zapped_row_count += 1

            #print( "Board height: ", len( self.board ) )
            while len( self.board ) < self.board_height:
                row = []
                for x in range( self.board_width ):
                    row.append( BOARD_BLOCK_NULL )

                self.board.insert( 0, row )
            #print( "Board height: ", len( self.board ) )

            this_drop_points = 0
            if zapped_row_count > 0:
                this_drop_points = ( 1 << zapped_row_count - 1 ) * 100
                self.score += this_drop_points + self.previous_drop_points
                changed_objects.score = True
                
                lines_zapped_this_level = self.lines_zapped_per_level.get( self.level, 0 )
                self.lines_zapped_per_level[ self.level ] = lines_zapped_this_level + zapped_row_count

                #print( "Lines zapped this level: ", self.lines_zapped_per_level[ self.level ] )
                if self.lines_zapped_per_level[ self.level ] >= self.lines_per_level_advance:
                    self.set_level( self.level + 1 )
                    changed_objects.level = True
                else:
                    pass
                    
            else:
                pass

            changed_objects.zapped_rows_sub_score = this_drop_points + self.previous_drop_points
            
        else:
            pass

        self.previous_drop_points = this_drop_points

        
    def get_block_index( self, board_x, board_y ):
        return self.board[ board_y ][ board_x ]


    def set_move_delta( self, delta_x, delta_y ):
        self.previous_move_time_ms = 0
        self.user_move_delta_x = delta_x
        self.user_move_delta_y = delta_y


    def move_active_tetromino( self, delta_x, delta_y ):
        moved_ok = False

        if self.is_running and not self.is_paused and ( ( delta_x != 0 ) or ( delta_y != 0 ) ):
            previous_left = self.active_tetromino.left
            previous_top = self.active_tetromino.top

            has_piece_moved = \
                ( previous_left != self.active_tetromino.left ) or \
                ( previous_top != self.active_tetromino.top )
            
            # We do two checks here. If we are trying to move with a
            # delta_x but are blocked because we are next to a wall, a valid
            # delta_y should still be actioned.
            if self.is_tetromino_location_ok( \
                self.active_tetromino_index, \
                    self.active_tetromino.current_instance, \
                    self.active_tetromino.left + delta_x, \
                    self.active_tetromino.top + delta_y \
                    ):

                self.active_tetromino.left += delta_x
                self.active_tetromino.top += delta_y
                moved_ok = True
                
            elif self.is_tetromino_location_ok( \
                self.active_tetromino_index, \
                    self.active_tetromino.current_instance, \
                    self.active_tetromino.left, \
                    self.active_tetromino.top + delta_y \
                    ):
                
                self.active_tetromino.top += delta_y
                moved_ok = True
                
            else:
                pass

            if self.active_tetromino.top != previous_top:
                self.previous_drop_time_ms = int( round( time.time() * 1000 ) )

        return moved_ok


    def rotate_active_tetromino( self ):
        rotated_ok = False
        
        if self.is_running and not self.is_paused:
            next_instance = self.active_tetromino.current_instance + 1
            if next_instance >= self.active_tetromino.instance_count:
                next_instance = 0
                
            if self.is_tetromino_location_ok( self.active_tetromino_index, \
                                              next_instance, \
                                              self.active_tetromino.left, \
                                              self.active_tetromino.top \
                                              ):
                self.active_tetromino.current_instance = next_instance
                rotated_ok = True
                
            elif ( self.active_tetromino.left < 0 ) \
                    or ( self.active_tetromino.left + self.active_tetromino.edge_length >= self.board_width ):
                # If we failed to rotate, but we are overlapping the edge of
                # the board then shift across and try to rotate again.
                test_x = self.active_tetromino.left
                if ( self.active_tetromino.left < 0 ):
                    delta_x = 1
                elif ( self.active_tetromino.left + self.active_tetromino.edge_length >= self.board_width ):
                    delta_x = -1
                else:
                    # Invalid!
                    delta_x = 0

                while not rotated_ok and ( ( test_x <= 0 ) or ( test_x + self.active_tetromino.edge_length >= self.board_width ) ):
                    #print( "Testing shift-then-rotate. x = ", test_x )
                    rotated_ok = self.is_tetromino_location_ok( \
                        self.active_tetromino_index, \
                            next_instance, \
                            test_x, \
                            self.active_tetromino.top \
                            )
                    if not rotated_ok:
                        test_x += delta_x

                if rotated_ok:
                    self.active_tetromino.current_instance = next_instance
                    self.active_tetromino.left = test_x
                else:
                    pass
                
            else:
                pass

        return rotated_ok


    def set_level( self, new_level ):
        self.level = new_level

        self.drop_delay = int( self.base_drop_delay / self.level )
        
        return self.level


    def next_iteration( self ):
        self.changed_objects.reset()
        
        if self.is_running and not self.is_paused:
            
            current_time_ms = int( round( time.time() * 1000 ) )
            if self.previous_time_ms == 0:
                self.previous_time_ms = current_time_ms
                self.previous_drop_time_ms = current_time_ms
            else:
                #print( current_time_ms, " : ", self.previous_drop_time_ms, " : ", current_time_ms - self.previous_drop_time_ms, " : ", self.drop_delay )
                # Check to see if we have a user-applied movement that needs to
                # be actioned.
                if ( self.user_move_delta_x != 0 ) or ( self.user_move_delta_y != 0 ):
                    if current_time_ms - self.previous_move_time_ms > self.user_move_delay:
                        previous_top = self.active_tetromino.top
                        
                        moved_ok = self.move_active_tetromino( self.user_move_delta_x, self.user_move_delta_y )
                        self.previous_move_time_ms = current_time_ms

                        # Points for soft dropping.
                        if moved_ok and ( self.user_move_delta_y > 0 ) and ( self.active_tetromino.top > previous_top ):
                            self.score += ( self.points_per_soft_drop * self.level )
                            self.changed_objects.score = True
                    else:
                        pass
                else:
                    pass
                

                # Check to see if we need to drop on block.
                if current_time_ms - self.previous_drop_time_ms > self.drop_delay:
                    if self.active_tetromino is not None:
                        if self.move_active_tetromino( 0, 1 ):
                            # The active piece has dropped OK
                            pass
                        else:
                            self.add_active_tetromino_to_board()
                            self.changed_objects.board = True

                            self.process_completed_rows( self.changed_objects )
                            self.set_next_active_tetromino()

                            # If the game state has changed to not running,
                            # then the game is over.
                            if not self.is_running:
                                self.changed_objects.game_over_state = True
                    
                    else:
                        # I don't think we should ever end up in here.
                        pass
                    
                    self.previous_drop_time_ms = current_time_ms
                else:
                    pass
                
        else:
            pass
            
        
        return self.changed_objects
    

# The game loop returns one of these, containing switches that define what key
# objects have changed. The host can then decide what it needs to render.
class ChangedObjects:
    current_active_tetromino = False
    board = False
    score = False
    level = False
    zapped_rows_sub_score = 0
    zapped_row_indices = None
    game_over_state = False

    
    def __init__( self ):
        self.reset()

        
    def reset( self ):
        self.current_active_tetromino = False
        self.board = False
        self.score = False
        self.level = False
        self.game_over_state = False

        # Is there a more efficient way of doing this (clearing the list)?
        self.zapped_row_indices = []
        
        self.zapped_rows_sub_score = 0
