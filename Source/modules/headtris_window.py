#
# game_window_gfx
#
# A GTK window with a single control - a drawing area.
WINDOW_GUI_FILE_NAME = "Headtris Game Window.glade"

KEY_ACTION_UNKNOWN = 0
KEY_ACTION_PRESSED = 1
KEY_ACTION_RELEASED = 2

KEY_STATE_UNKNOWN = 0
KEY_STATE_UP = 1
KEY_STATE_DOWN = 2

WINDOW_SPACING = 0.05
PIX_OVERFLOW = 0

# Initial size hint for the game drawing area. This width will be adjusted to
# give square blocks.
DEF_GAME_WIDTH = 480
DEF_GAME_HEIGHT = 480

# Game display update (ms)
# 40 ms is roughly 25 frames per second.
DEF_GAME_REFRESH_INTERVAL = 40

# Colours.
DEF_TETROMINO_SATURATION = 0.8
DEF_TETROMINO_VALUE = 0.9

DEF_FONT_NAME = "Droid Sans"

DEF_ANIMATION_INTERVAL = DEF_GAME_REFRESH_INTERVAL * 2
ANIM_PAUSE_DURATION = 150
ANIM_ROW_SCORE_DURATION = 1500
ANIM_GAME_OVER_DELAY = 1000
ANIM_SHOW_OVERLAY_DURATION = 250

OVERLAY_STATE_NONE = 0
OVERLAY_STATE_GAME_OVER = 1
OVERLAY_STATE_MAIN = 2
OVERLAY_STATE_KEYBINDINGS = 3

KEYDEF_CALLBACK = 0
KEYDEF_STROKE = 1
KEYDEF_LEFT = "Left"
KEYDEF_RIGHT = "Right"
KEYDEF_ROTATE = "Rotate"
KEYDEF_DROP = "Drop"
KEYDEF_PAUSE = "Pause"
KEYDEF_QUIT = "Quit"

import cairo
import threading
import sys
import math

from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GObject

# Local modules.
import headtris_game
import tetromino
import drawing
import animation
import key_binding

_ = None

class HeadtrisWindow:
    # General properties.
    game_refresh_interval = 0
    draw_grid = False
    show_next_piece = True
    project = None
    
    # The main game object.
    game = None


    # Widgets.
    window = None
    drawingarea_game = None


    # Surfaces.
    buffered_surface = None
    buffered_context = None
    background_surface = None
    background_context = None
    panel_surface = None
    panel_context = None
    board_surface = None
    board_context = None
    rubble_surface = None
    rubble_context = None

    # Overlay, for high scores, key-biding alterations, etc.
    overlay_surface = None
    overlay_context = None
    overlay_state = OVERLAY_STATE_MAIN
    is_overlay_dirty = True
    overlay_show_progress = 0.0
    overlay_alpha = 0.0
    

    # The tetrominoes themselves.
    tetromino_surface_size = 0
    tetromino_surfaces = None
    tetromino_contexts = None
    tetromino_centres = None
    tetromino_colours = None
    

    # Key bindings.
    key_bindings = None
    key_states = None
    piece_move_delta_x = 0
    piece_move_delta_y = 0


    # Mouse pointer.
    is_mouse_visible = True


    # Graphics layout.
    font_name = ""
    window_spacing  = 0
    drawingarea_width = 0
    drawingarea_height = 0
    panel_width = 0
    panel_height = 0
    panel_left = 0
    panel_top = 0
    board_width = 0
    board_height = 0
    board_left = 0
    board_top = 0
    font_size_small = 0
    font_size_standard = 0
    font_size_large = 0
    next_piece_left = 0
    next_piece_top = 0
    pixels_per_block_x = 0
    pixels_per_block_y = 0


    # Animation.
    anims = None
    pause_overlay_alpha = 0.0
    row_score_animations = []
    #game_over_overlay_alpha = 0.0


    def __init__( self, project ):
        # General properties.
        self.drawingarea_width = 0
        self.drawingarea_height = 0
        self.game_refresh_interval = DEF_GAME_REFRESH_INTERVAL
        self.font_name = DEF_FONT_NAME
        self.project = project
        global _
        _ = self.project.translation.gettext


        # Load the Glade XML GUI file and create the widgets.
        builder = Gtk.Builder()
        builder.add_from_file( self.project.get_data_file_name( WINDOW_GUI_FILE_NAME ) )

        # Store pointers to our widgets.
        self.window = builder.get_object( "game_window" )
        self.drawingarea_game = builder.get_object( "drawingarea_game" )

        # Set the window title.
        self.window.set_title( self.project.project_name )

        # Connect the signal handlers.
        handlers = {
            "on_game_window_delete_event" : self.on_delete_event,
            "on_drawingarea_game_draw" : self.on_drawingarea_game_draw,
            
            "on_game_window_key_press_event" : self.on_game_window_key_press_event,
            "on_game_window_key_release_event" : self.on_game_window_key_release_event,
            "on_game_window_event" : self.on_game_window_event
            }
        builder.connect_signals( handlers )

        self.initialise_key_bindings()

        # Make sure that we pick up mouse-move events.
        self.window.add_events( Gdk.EventMask.POINTER_MOTION_MASK )

        # Create the animation manager.
        self.anims = animation.AnimationManager()


    def set_game( self, new_game ):
        # This will force the pixels-per-block calculations to be remade and
        # the board grid will be redrawn.
        self.destroy_image_caches()
        self.game = new_game
        

    def initialise_key_bindings( self ):
        self.key_bindings = key_binding.KeyBindings()

        self.key_bindings.add( key_binding.KeyBinding( KEYDEF_LEFT, _( "Move left" ), self.key_left, [ "Left" ] ) )
        self.key_bindings.add( key_binding.KeyBinding( KEYDEF_RIGHT, _( "Move right" ), self.key_right, [ "Right" ] ) )
        self.key_bindings.add( key_binding.KeyBinding( KEYDEF_ROTATE, _( "Rotate current piece" ), self.key_rotate_piece, [ "Up" ] ) )
        self.key_bindings.add( key_binding.KeyBinding( KEYDEF_DROP, _( "Drop the current piece" ), self.key_drop, [ "Down" ] ) )
        self.key_bindings.add( key_binding.KeyBinding( KEYDEF_PAUSE, _( "Play or pause the game" ), self.key_play_pause, [ "p" ] ) )
        self.key_bindings.add( key_binding.KeyBinding( KEYDEF_QUIT, _( "Quit" ), self.key_quit, [ "q", "Escape" ] ) )
        
        self.key_states = {}
        

    def on_game_window_event( self, widget, event ):
        #print( event.type )
        if event.type == Gdk.EventType.MOTION_NOTIFY:
            if not self.is_mouse_visible:
                self.show_mouse_pointer()
            else:
                pass
        else:
            pass
            #print( event.type )
        

    def on_game_window_key_press_event( self, widget, event ):
        if ( self.is_mouse_visible ) and ( self.game is not None ) and ( self.game.is_running ) and ( not self.game.is_paused ):
            self.hide_mouse_pointer()
        else:
            pass

        key_name = Gdk.keyval_name( event.keyval )
        key_binding = self.key_bindings.find( key_name )

        if key_binding is not None:
            key_state = self.key_states.get( key_name, KEY_STATE_UNKNOWN )
            if key_state != KEY_STATE_DOWN:
                self.key_states[ key_name ] = KEY_STATE_DOWN
                key_binding.callback( KEY_ACTION_PRESSED )
            else:
                pass
        else:
            pass
            

    def on_game_window_key_release_event( self, widget, event ):
        key_name = Gdk.keyval_name( event.keyval )
        key_binding = self.key_bindings.find( key_name )
        
        if key_binding is not None:
            key_state = self.key_states.get( key_name, KEY_STATE_UNKNOWN )
            if key_state != KEY_STATE_UP:
                self.key_states[ key_name ] = KEY_STATE_UP
                key_binding.callback( KEY_ACTION_RELEASED )
            else:
                pass
        else:
            pass
            

    def on_timeout( self ):
        if self.game is not None:
            changed_objects = self.game.next_iteration()
            if changed_objects.board:
                self.redraw_rubble()

            if changed_objects.score or changed_objects.level:
                self.redraw_panel()

                # Start a floating row-score animation.
                if changed_objects.zapped_rows_sub_score > 0:
                    anim = FloatingTextAnimation( \
                        DEF_ANIMATION_INTERVAL, \
                            ANIM_ROW_SCORE_DURATION, \
                            self.anim_row_score_handler, \
                            self.drawingarea_game.get_window(), \
                            str( changed_objects.zapped_rows_sub_score ), \
                            self.font_size_large, \
                            self.board_left + ( self.board_width / 2 ), \
                            self.board_top + ( ( 2 * self.board_height ) / 3 ) \
                            )
                    self.anims.add_animation( anim, animation.AUTO_START )


            if changed_objects.game_over_state:
                self.overlay_state = OVERLAY_STATE_GAME_OVER
                self.show_overlay()
            else:
                pass
                
            # Process active animations.
            self.anims.next_iteration()

            self.drawingarea_game.queue_draw()
        else:
            pass

        return True
        

    def key_unknown( self, action ):
        # Stub.
        pass


    def key_drop( self, action ):
        if action == KEY_ACTION_PRESSED:
            self.piece_move_delta_y = 1
        else:
            if self.piece_move_delta_y > 0:
                self.piece_move_delta_y = 0
            else:
                pass

        self.set_piece_move_delta()


    def key_rotate_piece( self, action ):
        if action == KEY_ACTION_PRESSED:
            self.game.rotate_active_tetromino()
        else:
            pass


    def key_left( self, action ):
        if action == KEY_ACTION_PRESSED:
            self.piece_move_delta_x = -1
        else:
            if self.piece_move_delta_x < 0:
                self.piece_move_delta_x = 0
            else:
                pass

        self.set_piece_move_delta()


    def key_right( self, action ):
        if action == KEY_ACTION_PRESSED:
            self.piece_move_delta_x = 1
        else:
            if self.piece_move_delta_x > 0:
                self.piece_move_delta_x = 0
            else:
                pass

        self.set_piece_move_delta()


    def key_help( self, action ):
        if action == KEY_ACTION_PRESSED:
            print( _( "TODO: Implement help: " ) )
        else:
            pass


    def key_quit( self, action ):
        if action == KEY_ACTION_PRESSED:
            self.stop()
        else:
            pass


    def key_play_pause( self, action ):
        if action == KEY_ACTION_PRESSED:
            self.toggle_game_pause_state()
        else:
            pass


    def show_mouse_pointer( self ):
        gdk_window = self.drawingarea_game.get_window()
        gdk_window.set_cursor( Gdk.Cursor.new( Gdk.CursorType.ARROW ) )
        self.is_mouse_visible = True
        pass


    def hide_mouse_pointer( self ):
        gdk_window = self.drawingarea_game.get_window()
        gdk_window.set_cursor( Gdk.Cursor.new( Gdk.CursorType.BLANK_CURSOR ) )
        self.is_mouse_visible = False
        pass


    def start( self ):
        # FInd a window size that works for us.
        test_width = DEF_GAME_WIDTH
        test_height = DEF_GAME_HEIGHT
        self.calculate_all_dimensions( test_width, test_height )
        while self.panel_width <= 0:
            #test_width = test_width * 1.5
            test_height = test_height * 0.8
            self.calculate_all_dimensions( test_width, test_height )


        if ( self.panel_width > 0 ) and ( self.panel_height > 0 ):
            #print( "Using drawing area: ", test_width, " x ", test_height )
            self.drawingarea_game.set_size_request( \
                test_width, \
                    test_height )


        if self.window is not None:
            self.window.show_all()

            #print( "Setting game refresh interval to ", self.game_refresh_interval, "ms." )
            GObject.timeout_add( self.game_refresh_interval, self.on_timeout )

            self.show_overlay()
            
            #print( "Starting Gtk main loop." )
            Gtk.main()
        else:
            print( "start: Top-level windodw widget is missing!" )


    def show_overlay( self ):
        self.is_overlay_dirty = True
        self.overlay_alpha = 0.0
        delay = animation.AUTO_START
        duration = ANIM_SHOW_OVERLAY_DURATION
            
        anim_show_overlay = animation.Animation( DEF_ANIMATION_INTERVAL, duration, self.anim_show_overlay_handler )
        self.anims.add_animation( anim_show_overlay, delay )


    def hide_overlay( self ):
        self.overlay_alpha = 1.0
        delay = animation.AUTO_START
        duration = ANIM_SHOW_OVERLAY_DURATION
        if self.overlay_state == OVERLAY_STATE_GAME_OVER:
            delay = ANIM_GAME_OVER_DELAY
        else:
            delay = animation.AUTO_START
        anim_show_overlay = animation.Animation( DEF_ANIMATION_INTERVAL, duration, self.anim_hide_overlay_handler )
        self.anims.add_animation( anim_show_overlay, delay )


    def stop( self ):
        # Protect this if we go multi-threaded.
        if self.window is not None:
            #print( "Destroying window." )
            self.window.destroy()
            self.window = None
            
        #print( "Stopping Gtk main loop." )
        Gtk.main_quit()

        self.destroy_image_caches()


    def toggle_game_pause_state( self ):
        if self.game is None:
            pass
        elif not self.game.is_running:
            self.start_new_game()
        elif self.game.is_paused:
            self.game.resume()
            anim_pause = animation.Animation( DEF_ANIMATION_INTERVAL, ANIM_PAUSE_DURATION, self.anim_pause_handler )
            self.anims.add_animation( anim_pause, animation.AUTO_START )
        else:
            anim_pause = animation.Animation( DEF_ANIMATION_INTERVAL, ANIM_PAUSE_DURATION, self.anim_pause_handler )
            self.anims.add_animation( anim_pause, animation.AUTO_START )
            self.game.pause()
        
        
    def start_new_game( self ):
        if self.game.is_running:
            self.game.stop()

        self.hide_mouse_pointer()
            
        self.piece_move_delta_x = 0
        self.piece_move_delta_y = 0
        self.overlay_state = OVERLAY_STATE_NONE
        
        self.game.reset()
        self.redraw_board()
        self.redraw_rubble()
        self.redraw_panel()
        self.game.start()
        self.drawingarea_game.queue_draw()

        #self.pause_overlay_alpha = 0.0
        #self.game_over_overlay_alpha = 0.0

        self.hide_overlay()


    def stop_current_game( self ):
        if self.game.is_running:
            self.game.stop()
            
        self.drawingarea_game.queue_draw()


    def on_delete_event( self, widget, event ):
        #print( "Received delete event." )
        self.stop()


    def set_piece_move_delta( self ):
        self.game.set_move_delta( \
            self.piece_move_delta_x, \
            self.piece_move_delta_y )


    def get_window_spacing( self ):
        # Calculate the window_spacing property, but don't set it here.
        widget_size = self.drawingarea_game.get_allocation()

        return int( min( widget_size.width, widget_size.height ) * WINDOW_SPACING )


    def destroy_tetromino_image_caches( self ):
        if self.tetromino_surfaces is not None:
            #print( "Destroying tetromino surfaces." )
            for tetromino_index in range( len( self.tetromino_surfaces ) ):
                for instance_index in range( len( self.tetromino_surfaces[tetromino_index] ) ):
                    self.tetromino_surfaces[tetromino_index][instance_index].finish()
        
            self.tetromino_surfaces = None
            self.tetromino_contexts = None
        else:
            pass

    
    def destroy_image_caches( self ):
        self.destroy_tetromino_image_caches()
        
        
        if self.overlay_surface is not None:
            #print( "Destroying overlay surface." )
            self.overlay_surface.finish()
            self.overlay_surface = None
        else:
            pass
        

        if self.rubble_surface is not None:
            #print( "Destroying rubble surface." )
            self.rubble_surface.finish()
            self.rubble_surface = None
        else:
            pass
        

        if self.board_surface is not None:
            #print( "Destroying board surface." )
            self.board_surface.finish()
            self.board_surface = None
        else:
            pass
        

        if self.panel_surface is not None:
            #print( "Destroying panel surface." )
            self.panel_surface.finish()
            self.panel_surface = None
        else:
            pass
        

        if self.background_surface is not None:
            #print( "Destroying background surface." )
            self.background_surface.finish()
            self.background_surface = None
        else:
            pass
        

        if self.buffered_surface is not None:
            #print( "Destroying buffered surface." )
            self.buffered_surface.finish()
            self.buffered_surface = None
        else:
            pass


    def calculate_all_dimensions( self, display_width, display_height ):
        self.drawingarea_width = display_width
        self.drawingarea_height = display_height

        self.window_spacing = self.get_window_spacing()

        self.board_height = int( self.drawingarea_height - ( 2 * self.window_spacing ) )
        self.pixels_per_block_y = int( self.board_height / self.game.board_height )
        self.pixels_per_block_x = self.pixels_per_block_y
        self.board_width = self.game.board_width * self.pixels_per_block_x
        self.board_height = self.game.board_height * self.pixels_per_block_y
        
        #print( "Block size: ", self.pixels_per_block_x, " x ", self.pixels_per_block_y )
            
        self.board_left = self.drawingarea_width - self.board_width - self.window_spacing
        self.board_top = int( ( self.drawingarea_height - self.board_height ) / 2 )

        self.panel_width = self.drawingarea_width - self.board_width - ( 3 * self.window_spacing )
        self.panel_height = self.board_height
        self.panel_left = self.window_spacing
        self.panel_top = self.board_top

        self.next_piece_left = 0
        self.next_piece_top = 0
        
        self.font_size_standard = int( self.board_width / 12 )
        self.font_size_small = int( self.font_size_standard * 0.75 )
        self.font_size_large = int( self.font_size_standard * 2 )

        #print( "Panel width: ", self.panel_width )
        

    def rebuild_image_caches( self ):
        # Start with a clean slate.
        self.destroy_image_caches()
        
        gdk_window = self.drawingarea_game.get_window()
        widget_size = self.drawingarea_game.get_allocation()
        self.calculate_all_dimensions( widget_size.width, widget_size.height )
        
        #print( "Rebuilding image caches." )
        
        if self.buffered_surface is None:
            #print( "Creating off-screen drawing surface." )
            self.buffered_surface = gdk_window.create_similar_surface( cairo.CONTENT_COLOR_ALPHA, \
                                                                           self.drawingarea_width, \
                                                                           self.drawingarea_height )
            self.buffered_context = cairo.Context( self.buffered_surface )
            self.buffered_context.select_font_face( self.font_name, cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_BOLD )
        else:
            pass

        
        if self.background_surface is None:
            #print( "Creating background surface." )
            self.background_surface = gdk_window.create_similar_surface( cairo.CONTENT_COLOR_ALPHA, \
                                                                             self.drawingarea_width, \
                                                                             self.drawingarea_height )
            self.background_context = cairo.Context( self.background_surface )

            # Paint everything black.
            self.background_context.set_source_rgb( 0.0, 0.0, 0.0 )
            self.background_context.rectangle( 0, 0, self.drawingarea_width, self.drawingarea_height )
            self.background_context.paint()

            # Draw a linear gradient.
            pattern = cairo.LinearGradient( 0, 0, 0.0, self.drawingarea_height )
            pattern.add_color_stop_rgba( 1.0, 1.0, 1.0, 0.75, 0.5 )
            pattern.add_color_stop_rgba( 0.0, 0.8, 0.8, 0.5, 0.0 )
            self.background_context.set_source( pattern )
            self.background_context.fill()

            #self.unit_px_width = self.board_size.width / self.game.board_width
            #self.unit_px_height = self.board_size.height / self.game.board_height
        else:
            pass

        
        if ( self.board_surface is None ) and ( self.board_width > 0 ) and ( self.board_height > 0 ):
            #print( "Creating board surface." )
            self.board_surface = gdk_window.create_similar_surface( cairo.CONTENT_COLOR_ALPHA, \
                                                                        self.board_width, \
                                                                        self.board_height )
            self.board_context = cairo.Context( self.board_surface )
            
            self.redraw_board()
        else:
            pass


        # Create the tetromino images themselves.
        self.create_tetromino_image_caches()


        if ( self.panel_surface is None ) and ( self.panel_width > 0 ) and ( self.panel_height > 0 ):
            #print( "Creating panel surface." )
            self.panel_surface = gdk_window.create_similar_surface( \
                cairo.CONTENT_COLOR_ALPHA, \
                self.panel_width, \
                self.panel_height )
            self.panel_context = cairo.Context( self.panel_surface )
            
            self.panel_context.select_font_face( self.font_name, cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_BOLD )
            self.redraw_panel()
        else:
            pass

        
        if ( self.rubble_surface is None ) and ( self.board_width > 0 ) and ( self.board_height > 0 ):
            #print( "Creating rubble surface." )
            self.rubble_surface = gdk_window.create_similar_surface( cairo.CONTENT_COLOR_ALPHA, \
                                                                        self.board_width, \
                                                                        self.board_height )
            self.rubble_context = cairo.Context( self.rubble_surface )
            self.redraw_rubble()
        else:
            pass


        if self.overlay_surface is None:
            #print( "Creating overlay drawing surface." )
            self.overlay_surface = gdk_window.create_similar_surface( \
                cairo.CONTENT_COLOR_ALPHA, \
                    self.drawingarea_width, \
                    self.drawingarea_height )
            self.overlay_context = cairo.Context( self.overlay_surface )
            self.is_overlay_dirty = True
        else:
            pass

        
    def redraw_board( self ):
        if self.board_surface is not None:
            self.board_context.set_operator( cairo.OPERATOR_CLEAR )
            self.board_context.fill()
            
            self.board_context.set_operator( cairo.OPERATOR_SOURCE )
            self.board_context.set_source_rgba( 0.0, 0.0, 0.0, 0.4 )
            self.board_context.rectangle( 0, 0, self.board_width, self.board_height )
            self.board_context.fill()

            self.board_context.set_operator( cairo.OPERATOR_SOURCE )
            # Draw the grid.
            if ( self.game is not None ) and self.draw_grid:
                self.board_context.set_source_rgba( 0.0, 0.0, 0.0, 0.7 )
                self.board_context.set_line_width( 1.0 )
                for block_y in range( self.game.board_height ):
                    pix_y = block_y * self.pixels_per_block_y
                    self.board_context.move_to( 0, pix_y )
                    self.board_context.line_to( self.board_width, pix_y )

                for block_x in range( self.game.board_width ):
                    pix_x = block_x * self.pixels_per_block_x
                    self.board_context.move_to( pix_x, 0 )
                    self.board_context.line_to( pix_x, self.board_height )

                self.board_context.stroke()
            else:
                pass
        else:
            pass


    def redraw_panel( self ):
        if self.panel_surface is not None:
            #print( "Redrawing panel." )
            
            self.panel_context.set_operator( cairo.OPERATOR_CLEAR )
            self.panel_context.fill()
            
            self.panel_context.set_operator( cairo.OPERATOR_SOURCE )
            self.panel_context.set_source_rgba( 0.0, 0.0, 0.0, 0.4 )
            drawing.rounded_rect( \
                self.panel_context, \
                0, \
                0, \
                self.panel_width, \
                self.panel_height, \
                self.window_spacing )
            self.panel_context.fill()

            # Text colour.
            self.panel_context.set_source_rgb( 1.0, 1.0, 1.0 )

            centre_x = self.panel_width / 2
            top_y = self.window_spacing
            
            self.panel_context.set_font_size( self.font_size_standard )
            top_y += self.font_size_standard
            drawing.centre_text_x( self.panel_context, centre_x, top_y, _( "Next Piece" ) )

            # The next piece.
            #top_y += self.font_size_standard + self.window_spacing

            # Sub-panel to hold the next piece.
            top_y += self.font_size_small + ( self.pixels_per_block_y / 2 )

            # TODO: Check this. I think we're miscalculating the vertical centre point.
            self.next_piece_top = top_y + self.tetromino_surface_size - ( self.pixels_per_block_y / 2 )
            self.panel_context.set_operator( cairo.OPERATOR_ATOP )
            self.panel_context.set_source_rgba( 1.0, 1.0, 1.0, 0.2 )
            drawing.rounded_rect( \
                self.panel_context, \
                ( ( self.panel_width - self.tetromino_surface_size ) / 2 ) - ( self.pixels_per_block_x / 2 ), \
                top_y - ( self.pixels_per_block_y / 2 ), \
                self.tetromino_surface_size + ( 1 * self.pixels_per_block_x ), \
                self.tetromino_surface_size + ( 1 * self.pixels_per_block_y ), \
                self.pixels_per_block_x / 2 )
            self.panel_context.fill()
            top_y += self.tetromino_surface_size + self.font_size_standard

            self.panel_context.set_operator( cairo.OPERATOR_SOURCE )
            self.panel_context.set_source_rgba( 1.0, 1.0, 1.0, 1.0 )

            top_y += self.font_size_small + self.font_size_large
            drawing.centre_text_x( self.panel_context, centre_x, top_y, _( "Score" ) )

            top_y += self.font_size_small / 2.0
            self.panel_context.set_font_size( self.font_size_large )
            top_y += self.font_size_large
            drawing.centre_text_x( self.panel_context, centre_x, top_y, str( self.game.score ) )

            top_y += self.font_size_large
            
            self.panel_context.set_font_size( self.font_size_standard )
            top_y += self.font_size_standard
            drawing.centre_text_x( self.panel_context, centre_x, top_y, _( "Level" ) )

            top_y += self.font_size_small / 2.0
            self.panel_context.set_font_size( self.font_size_large )
            top_y += self.font_size_large
            drawing.centre_text_x( self.panel_context, centre_x, top_y, str( self.game.level ) )
        else:
            pass

            
    def redraw_rubble( self ):
        if self.rubble_surface is not None:
            self.rubble_context.rectangle( 0, 0, self.board_width, self.board_height )
            self.rubble_context.set_operator( cairo.OPERATOR_CLEAR )
            self.rubble_context.fill()
            
            if self.game is not None:
                #print( "Redrawing rubble." )
                self.rubble_context.set_operator( cairo.OPERATOR_SOURCE )
                for board_y in range( 0, self.game.board_height ):
                    py = board_y * self.pixels_per_block_y
                    
                    for board_x in range( 0, self.game.board_width ):
                        px = board_x * self.pixels_per_block_x
                        
                        block_index = self.game.get_block_index( board_x, board_y )
                        
                        if block_index != headtris_game.BOARD_BLOCK_NULL:
                            block_colour = self.tetromino_colours[ block_index ]
                            
                            self.rubble_context.set_source_rgb( block_colour.red, block_colour.green, block_colour.blue )
                            self.rubble_context.rectangle( \
                                px - PIX_OVERFLOW, \
                                py - PIX_OVERFLOW, \
                                self.pixels_per_block_x + ( 2 * PIX_OVERFLOW ), \
                                self.pixels_per_block_y + ( 2 * PIX_OVERFLOW ) )
                            self.rubble_context.fill()
                        else:
                            pass
                
            else:
                pass
        else:
            pass


    def create_tetromino_image_caches( self ):
        gdk_window = self.drawingarea_game.get_window()

        if self.tetromino_surfaces is None:
            # Create the colour table that we will use, based on even points
            # throughout the spectrum according to how many tetromino templates
            # we have.
            self.tetromino_colours = []

            # The centre-points for our tetrominoes/instances.
            self.tetromino_centres = []
            
            #print( "Creating tetromino surfaces." )
            #print( "Pixels per block: ", self.pixels_per_block_x, " x ", self.pixels_per_block_y )
            self.tetromino_surfaces = []
            self.tetromino_contexts = []
            colour_index = 0
            max_surface_width = 0
            max_surface_height = 0
            for tetromino_index in range( len( self.game.tetromino_templates ) ):
                # Create the colour object for this tetromino.
                #hue = tetromino_index * ( ( 2 * 3.541 ) / len( self.game.tetromino_templates ) )
                hue = tetromino_index * ( 1.0 / len( self.game.tetromino_templates ) )
                #print( "Idx/Hue: ", tetromino_index, " / ", hue )
                
                self.tetromino_colours.append( \
                    drawing.hsv_to_colour( \
                        hue, \
                            DEF_TETROMINO_SATURATION, \
                            DEF_TETROMINO_VALUE, \
                            1.0 \
                            ) \
                        )
                    
                
                piece = self.game.tetromino_templates[tetromino_index]
                piece_colour = self.tetromino_colours[colour_index]
                #print( "Colour index: ", colour_index )
                self.tetromino_surfaces.append( [] )
                self.tetromino_contexts.append( [] )
                self.tetromino_centres.append( [] )
                for instance_index in range( piece.instance_count ):
                    max_surface_width = max( piece.edge_length * self.pixels_per_block_x, max_surface_width )
                    max_surface_height = max( piece.edge_length * self.pixels_per_block_y, max_surface_height )
                    
                    #print( "Creating tetromino surface ", tetromino_index )
                    tetromino_surface = gdk_window.create_similar_surface( \
                        cairo.CONTENT_COLOR_ALPHA, \
                            ( piece.edge_length * self.pixels_per_block_x ) + ( 2 * PIX_OVERFLOW ), \
                            ( piece.edge_length * self.pixels_per_block_y ) + ( 2 * PIX_OVERFLOW ) )

                    # Used for calculating the centre point.
                    min_sub_x = piece.edge_length * self.pixels_per_block_x
                    max_sub_x = 0.0
                    min_sub_y = piece.edge_length * self.pixels_per_block_y
                    max_sub_y = 0.0

                    # Render the piece.
                    cairo_context = cairo.Context( tetromino_surface )
                    cairo_context.set_source_rgb( piece_colour.red, \
                                                  piece_colour.green, \
                                                  piece_colour.blue )
                    for sub_y in range( piece.edge_length ):
                        for sub_x in range( piece.edge_length ):
                            if piece.instances[instance_index][sub_y][sub_x:sub_x+1] == tetromino.BLOCK_ON_CHAR:
                                #print( "O", end="" )
                                px = ( sub_x * self.pixels_per_block_x ) - PIX_OVERFLOW
                                py = ( sub_y * self.pixels_per_block_y ) - PIX_OVERFLOW
                                pw = self.pixels_per_block_x + ( 2 * PIX_OVERFLOW )
                                ph = self.pixels_per_block_y + ( 2 * PIX_OVERFLOW )
                                
                                min_sub_x = min( px, min_sub_x )
                                max_sub_x = max( px + pw, max_sub_x )
                                min_sub_y = min( py, min_sub_y )
                                max_sub_y = max( py + ph, max_sub_y )
                                
                                cairo_context.rectangle( px, py, pw, ph )
                                cairo_context.fill()
                                
                            else:
                                pass
                                #print( " ", end="" )

                        #print( "" )

                    #print( "" )

                    self.tetromino_contexts[tetromino_index].append( cairo_context )
                    self.tetromino_surfaces[tetromino_index].append( tetromino_surface )

                    self.tetromino_centres[tetromino_index].append( drawing.Point( \
                            min_sub_x + ( ( max_sub_x - min_sub_x ) / 2 ), \
                                min_sub_y + ( ( max_sub_y - min_sub_y ) / 2 ) ) )

                colour_index += 1
                if colour_index > len( self.tetromino_colours ):
                    colour_index = 0

            # Set the width/height of the "next piece" drawing area to be the
            # size of one of our tetromino instance surfaces.
            #if max_surface_width > 0 and max_surface_height > 0:
            #    self.next_piece_canvas.set_size_request( max_surface_width, max_surface_height )
            if ( max_surface_width > 0 ) and ( max_surface_height > 0 ):
                self.next_piece_left = self.panel_left + ( ( self.panel_width - max_surface_width ) / 2 )
                self.tetromino_surface_size = max_surface_width
            else:
                print( "Zero max width/height!" )


    def redraw_overlay( self ):
        #self.overlay_context.rectangle( 0, 0, self.board_width, self.board_height )
        #self.overlay_context.set_operator( cairo.OPERATOR_CLEAR )
        #self.overlay_context.fill()
        
        self.overlay_context.set_operator( cairo.OPERATOR_SOURCE )
        self.overlay_context.set_source_rgba( 0.0, 0.0, 0.0, 0.9 )
        self.overlay_context.paint()

        text_x = self.window_spacing
        text_y = self.window_spacing
        
        self.overlay_context.set_source_rgba( 1.0, 1.0, 1.0, 1.0 )

        if self.overlay_state != OVERLAY_STATE_GAME_OVER:
            # Render the game name.
            self.overlay_context.set_font_size( self.font_size_large )
            text_y += self.font_size_large
            drawing.centre_text_x( self.overlay_context, self.drawingarea_width / 2, text_y, self.project.project_name )
            
            # Render the tag line.
            text_y += self.font_size_standard
            self.overlay_context.set_font_size( self.font_size_small )
            drawing.centre_text_x( self.overlay_context, self.drawingarea_width / 2, text_y, _( self.project.description_brief ) )

            text_y += self.font_size_standard
        
            self.overlay_context.set_font_size( self.font_size_small )
        else:
            pass
        
        if self.overlay_state == OVERLAY_STATE_MAIN:
            # Render key bindings.
            box_height = \
                ( len( self.key_bindings.key_bindings ) * self.font_size_standard ) + \
                ( ( len( self.key_bindings.key_bindings ) - 1 ) * self.font_size_standard / 2 ) + \
                ( 2 * self.window_spacing )
            self.overlay_context.set_operator( cairo.OPERATOR_SOURCE )
            self.overlay_context.set_source_rgba( 0.1, 0.1, 0.1, 0.8 )
            drawing.rounded_rect( \
                self.overlay_context, \
                    self.window_spacing, \
                    text_y, \
                    self.drawingarea_width - ( 2 * self.window_spacing ), \
                    box_height, \
                    self.window_spacing \
                    )
            self.overlay_context.fill()
            self.overlay_context.set_source_rgba( 1.0, 1.0, 1.0, 1.0 )
            text_x += self.window_spacing
            text_y -= ( self.font_size_standard / 2 ) - self.window_spacing
            for key_binding in self.key_bindings.key_bindings:
                text_y += self.font_size_standard * 1.5
                
                # Write the key description.
                self.overlay_context.move_to( text_x, text_y )
                self.overlay_context.show_text( key_binding.description )

                # Write the key stroke name.
                self.overlay_context.move_to( self.drawingarea_width - ( 2 * self.window_spacing ) - ( 3 * self.font_size_standard ), text_y )
                self.overlay_context.show_text( _( key_binding.key_names[0] ) )
                

        elif self.overlay_state == OVERLAY_STATE_GAME_OVER:
            self.overlay_context.set_source_rgba( 1.0, 1.0, 1.0, 1.0 )
            self.overlay_context.set_font_size( self.font_size_large )
            drawing.centre_text_xy( \
                self.overlay_context, \
                    self.drawingarea_width / 2, \
                    self.drawingarea_height / 2, \
                    _( "Game Over" ) )
            

            
        else:
            pass

        self.is_overlay_dirty = False


    def anim_pause_handler( self, animation ):
        if self.game.is_paused:
            self.pause_overlay_alpha = animation.progress
        else:
            self.pause_overlay_alpha = 1.0 - animation.progress


    def anim_show_overlay_handler( self, animation ):
        self.overlay_show_progress = animation.progress
        self.overlay_alpha = animation.progress

        if ( animation.progress >= 1.0 ) and ( self.overlay_state == OVERLAY_STATE_GAME_OVER ):
            self.hide_overlay()
        else:
            pass


    def anim_hide_overlay_handler( self, animation ):
        self.overlay_show_progress = animation.progress
        self.overlay_alpha = 1.0 - animation.progress
        
        if ( animation.progress >= 1.0 ) and ( self.overlay_state == OVERLAY_STATE_GAME_OVER ):
            self.overlay_state = OVERLAY_STATE_MAIN
            self.show_overlay()
        else:
            pass


    def anim_row_score_handler( self, animation ):
        animation.top = int( animation.start_top - ( ( self.board_height / 2 ) * animation.progress ) )
        #print( "Row score anim." )


    def on_drawingarea_game_draw( self, widget, cairo_context ):
        # We don't bother using the 'widget' parameter here, as we will be
        # drawing to drawingarea_game.
        widget_size = self.drawingarea_game.get_allocation()

        # Do we need to rebuild cached images?
        if ( widget_size.width != self.drawingarea_width ) or \
                ( widget_size.height != self.drawingarea_height ) or \
                ( self.buffered_surface is None ):
            self.rebuild_image_caches()
        else:
            pass

        # Copy the base in source mode.
        self.buffered_context.set_operator( cairo.OPERATOR_SOURCE )

        # Copy the background surface to the buffered surface first.
        self.buffered_context.set_source_surface( self.background_surface )
        self.buffered_context.rectangle( 0, 0, self.drawingarea_width, self.drawingarea_height )
        self.buffered_context.fill()

        # All other operations on the buffered surface are in mode:
        # OPERATOR_ATOP
        self.buffered_context.set_operator( cairo.OPERATOR_ATOP )
        
        # Copy the panel surface to the buffered surface.
        self.buffered_context.set_source_surface( self.panel_surface, self.panel_left, self.panel_top )
        self.buffered_context.rectangle( self.panel_left, self.panel_top, self.panel_width, self.panel_height )
        self.buffered_context.fill()
        

        # Copy the board surface to the buffered surface.
        self.buffered_context.set_source_surface( self.board_surface, self.board_left, self.board_top )
        self.buffered_context.rectangle( self.board_left, self.board_top, self.board_width, self.board_height )
        self.buffered_context.fill()


        # Copy the rubble to the board.
        self.buffered_context.set_source_surface( self.rubble_surface, self.board_left, self.board_top )
        self.buffered_context.rectangle( self.board_left, self.board_top, self.board_width, self.board_height )
        self.buffered_context.fill()


        if self.game is not None:
            if ( len( self.tetromino_surfaces ) > 0 ) and self.game.is_running:
                if self.show_next_piece:
                    # Copy the 'next piece' tetromino surface to the panel.
                    piece_surface = self.tetromino_surfaces[ self.game.next_tetromino_index ][ self.game.next_tetromino_instance_index ]
                    self.buffered_context.set_operator( cairo.OPERATOR_ATOP )

                    centre_point = self.tetromino_centres[ self.game.next_tetromino_index ][ self.game.next_tetromino_instance_index ]
                    self.next_piece_left = self.panel_left + ( self.panel_width / 2 ) - centre_point.x
                    
                    self.buffered_context.set_source_surface( \
                        piece_surface, \
                            self.next_piece_left, \
                            self.next_piece_top - centre_point.y )

                    self.buffered_context.rectangle( \
                        self.next_piece_left, \
                            self.next_piece_top - centre_point.y, \
                            self.game.tetromino_templates[ self.game.next_tetromino_index ].edge_length * self.pixels_per_block_x, \
                            self.game.tetromino_templates[ self.game.next_tetromino_index ].edge_length * self.pixels_per_block_y )
                
                    self.buffered_context.fill()
                else:
                    # Consider moving this into draw_panel().
                    self.panel_context.set_font_size( self.font_size_large )
                    drawing.centre_text_x( self.panel_context, self.panel_width / 2, self.next_piece_top, "?" )
        

            if self.game.active_tetromino is not None:
                # Establish a clip region stop prevent pieces from appearing
                # outside the board area.
                self.buffered_context.rectangle( \
                    self.board_left, \
                    self.board_top + ( 1 + PIX_OVERFLOW ), \
                    self.board_width, \
                    self.board_height - ( 1 + PIX_OVERFLOW ) )
                self.buffered_context.clip()
                
                # Render the current tetromino to the board.
                piece = self.game.active_tetromino
                piece_surface = self.tetromino_surfaces[self.game.active_tetromino_index][piece.current_instance]
                self.buffered_context.set_operator( cairo.OPERATOR_ATOP )

                if True:
                    self.buffered_context.set_source_surface( \
                        piece_surface, \
                            self.board_left + ( piece.left * self.pixels_per_block_x ) - PIX_OVERFLOW, \
                            self.board_top + ( piece.top * self.pixels_per_block_y ) - PIX_OVERFLOW )
                
                    self.buffered_context.rectangle( \
                        self.board_left + ( piece.left * self.pixels_per_block_x ) - PIX_OVERFLOW, \
                            self.board_top + ( piece.top * self.pixels_per_block_y ) - PIX_OVERFLOW, \
                            ( piece.edge_length * self.pixels_per_block_x ) + ( 2 * PIX_OVERFLOW ), \
                            ( piece.edge_length * self.pixels_per_block_y ) + ( 2 * PIX_OVERFLOW ) )

                self.buffered_context.fill()

                # Remove the clipping.
                self.buffered_context.reset_clip()
            else:
                pass


            # Render floating text objects.
            #self.buffered_context.set_operator( cairo.OPERATOR_SOURCE )
            for animation in self.anims.animations:
                if animation.__class__.__name__ == "FloatingTextAnimation":
                    if animation.surface is not None:
                        self.buffered_context.set_source_surface( \
                                                                  animation.surface, \
                                                                  animation.left, \
                                                                  animation.top )

                        self.buffered_context.rectangle( \
                                                         animation.left, \
                                                         animation.top, \
                                                         animation.width, \
                                                         animation.height )

                        self.buffered_context.paint_with_alpha( 1.0 - animation.progress )
                    else:
                        pass
                else:
                    pass


            # Render the "Game Paused" overlay.
            if self.game.is_paused or ( self.pause_overlay_alpha > 0.0 ):
                self.buffered_context.set_operator( cairo.OPERATOR_ATOP )
                self.buffered_context.set_source_rgba( 0.0, 0.0, 0.0, self.pause_overlay_alpha * 0.8 )
                self.buffered_context.rectangle( 0, 0, self.drawingarea_width, self.drawingarea_height )
                self.buffered_context.fill()

                self.buffered_context.set_source_rgba( 1.0, 1.0, 1.0, self.pause_overlay_alpha * 0.8 )
                self.buffered_context.set_font_size( \
                    self.font_size_large * ( self.pause_overlay_alpha + ( self.pause_overlay_alpha / 2 ) ) * 1.5 )
                drawing.centre_text_xy( \
                    self.buffered_context, \
                        self.drawingarea_width / 2, \
                        self.drawingarea_height / 2, \
                        _( "Paused" ) )
                
                self.buffered_context.set_source_rgba( 1.0, 1.0, 1.0, 1.0 )
                
            else:
                pass

            
            # If the game is not running then we need to render the main
            # overlay.
            #if ( not self.game.is_running ):
            if self.overlay_alpha > 0.0:
                if self.is_overlay_dirty:
                    self.redraw_overlay()
                else:
                    pass

                self.buffered_context.set_operator( cairo.OPERATOR_ATOP )
                self.buffered_context.set_source_surface( self.overlay_surface )
                self.buffered_context.rectangle( 0, 0, self.drawingarea_width, self.drawingarea_height )
                self.buffered_context.paint_with_alpha( self.overlay_alpha )
            else:
                pass

            #else:
            #    pass

        else:
            pass
                        
        
        # Render the game to the drawing area.
        cairo_context.set_operator( cairo.OPERATOR_SOURCE )
        cairo_context.set_source_surface( self.buffered_surface )
        cairo_context.rectangle( 0, 0, widget_size.width, widget_size.height )
        cairo_context.fill()


class FloatingTextAnimation( animation.Animation ):
    surface = None
    context = None
    left = 0
    top = 0
    start_left = 0
    start_top = 0
    width = 0
    height = 0
    text = ""

    
    def __init__( self, interval, duration, callback, gdk_window, text, font_size, centre_x, centre_y ):
        super().__init__( interval, duration, callback )

        # TODO: More efficient method?
        # Create a test surface to get our text metrics. Is there a better way
        # of doing this?
        test_surface = gdk_window.create_similar_surface( \
            cairo.CONTENT_COLOR_ALPHA, \
                100, \
                100 )
        test_context = cairo.Context( test_surface )
        test_context.select_font_face( DEF_FONT_NAME, cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_BOLD )
        test_context.set_font_size( font_size )

        ( x, y, text_width, text_height, dx, dy ) = test_context.text_extents( text )

        test_surface.finish()
        test_context = None

        self.width = int( text_width + ( font_size * 2 ) )
        self.height = int( text_height + ( font_size * 2 ) )
        self.surface = gdk_window.create_similar_surface( \
            cairo.CONTENT_COLOR_ALPHA, \
                self.width, \
                self.height )

        self.context = cairo.Context( self.surface )

        #self.context.set_operator( cairo.OPERATOR_SOURCE )
        #self.context.set_source_rgba( 0.0, 0.0, 0.0, 0.0 )
        #self.context.paint()

        #self.context.set_operator( cairo.OPERATOR_ATOP )

        # Black outline.
        #self.context.set_source_rgba( 0.0, 0.0, 0.0, 1.0 )
        #self.context.set_font_size( font_size + 1 )
        #drawing.centre_text_xy( self.context, self.width / 2, self.height / 2, text )

        # Main text.
        self.context.set_source_rgb( 1.0, 1.0, 1.0 )
        self.context.select_font_face( DEF_FONT_NAME, cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_BOLD )
        self.context.set_font_size( font_size )
        drawing.centre_text_xy( self.context, int( self.width / 2 ), int( self.height / 2 ), text )

        #drawing.rounded_rect( self.context, 5, 5, 80, 80, 5 )
        #self.context.fill()

        self.start_left = int( centre_x - ( self.width / 2 ) )
        self.start_top = int( centre_y - ( self.height / 2 ) )

        self.left = self.start_left
        self.top = self.start_top


    def stop( self ):
        super().stop()

        if self.surface is not None:
            self.surface.finish()
            self.surface = None
            self.context = None
        else:
            pass
