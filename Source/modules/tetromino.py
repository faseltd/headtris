#
# tetromino.py
#
BLOCK_ON_CHAR = "x"
BLOCK_OFF_CHAR = "."

class Tetromino:
    edge_length = 0
    instance_count = 0
    current_instance = 0

    original_row_strings = []

    instances = []
    left = 0
    top = 0

    def __init__( self, edge_length, row_strings ):
        self.edge_length = edge_length
        self.original_row_strings = []

        # TODO: Test length of the strings within row_strings.
        self.instance_count = 1 #instance_count

        row_length = 0
        if edge_length == 0:
            raise Exception( "edge_length can not be zero." )
        elif len( row_strings ) != edge_length:
            raise Exception( "Invalid number of rows. row_strings has ", \
                             len( row_strings ), \
                             " rows but edge_length is ", \
                             edge_length )
        else:
            row_length = len( row_strings[0] )
            if row_length == 0:
                raise Exception( "Can not have zero row length." )
            elif ( row_length % edge_length ) != 0:
                raise Exception( "The row length should be a factor of the edge length." )
            
            row_index = 0

            for row_index in range( self.edge_length ):
                test_length = len( row_strings[row_index] )
                if test_length != row_length:
                    raise Exception( "Row index ", \
                                     row_index, \
                                     " is not the correct row length (", \
                                     test_length, \
                                     "). It should be ", \
                                     row_length, \
                                     "." )
                else:
                    self.original_row_strings.append( row_strings[row_index] )

        self.instance_count = int( row_length / self.edge_length )

        #print( "Creating tetromino. ", end="" )
        #print( "Edge Length = ", self.edge_length, " ", end="" )
        #print( "Instance Count = ", self.instance_count, " ", end="" )
        #print()

        self.instances = []
        for instance_index in range( self.instance_count ):
            instance = []
            x_offset = instance_index * self.edge_length
            for row_index in range( self.edge_length ):
                instance.append( row_strings[ row_index ][ x_offset : x_offset + self.edge_length ] )

            self.instances.append( instance )

            #print( "\n".join( instance ) )


    #def clone( self ):
    #    print( "Clone: ", len( self.original_row_strings ) )
    #    return Tetromino( self.edge_length, self.original_row_strings )

