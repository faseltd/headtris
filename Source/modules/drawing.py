#
# drawing.py
#
# General useful drawing functions.
#
import cairo
import colorsys

from math import pi
from gi.repository import Gtk
from gi.repository import Gdk


def rounded_rect( cairo_context, left, top, width, height, radius ):
    cairo_context.move_to( left + radius, top )
    cairo_context.line_to( left + width - radius, top )
    cairo_context.arc( left + width - radius, top + radius, radius, 3 * pi / 2.0, 0.0 )
    cairo_context.line_to( left + width, top + height - radius )
    cairo_context.arc( left + width - radius, top + height - radius, radius, 0.0, pi / 2.0 )
    cairo_context.line_to( left + radius, top + height )
    cairo_context.arc( left + radius, top + height - radius, radius, pi / 2.0, pi )
    cairo_context.line_to( left, top + radius )
    cairo_context.arc( left + radius, top + radius, radius, pi, 3 * pi / 2.0 )


def centre_text_x( cairo_context, origin_x, origin_y, text ):
    ( x, y, width, height, dx, dy ) = cairo_context.text_extents( text )

    text_x = origin_x - ( width / 2.0 )
    text_y = origin_y
    cairo_context.move_to( text_x, text_y )
    cairo_context.show_text( text )


def centre_text_xy( cairo_context, origin_x, origin_y, text ):
    ( x, y, width, height, dx, dy ) = cairo_context.text_extents( text )

    text_x = origin_x - ( width / 2.0 )
    text_y = origin_y + ( height / 2.0 )
    cairo_context.move_to( text_x, text_y )
    cairo_context.show_text( text )


def hsv_to_colour( hue, saturation, value, alpha ):
    red, green, blue = colorsys.hsv_to_rgb( hue, saturation, value )
    
    return Colour( red, green, blue, alpha )


class Point:
    x = 0
    y = 0

    def __init__( self, x, y ):
        self.x = x
        self.y = y
    
#
# Define a colour with RGB values.
#
class Colour:
    red = 0.0
    green = 0.0
    blue = 0.0
    alpha = 0.0

    def __init__( self, red, green, blue, alpha ):
        self.red = red
        self.green = green
        self.blue = blue
        self.alpha = alpha
