#
# key_binding.py
#
class KeyBinding:
    name = ""
    description = ""
    callback = None
    key_names = []
    
    
    def __init__( self, name, description, callback, key_names ):
        self.name = name
        self.description = description
        self.callback = callback
        self.key_names = key_names
        

class KeyBindings:
    key_bindings = None
    dictionary = None
    is_dictionary_dirty = True

    
    def __init__( self ):
        self.key_bindings = []
        self.dictionary = {}
        self.is_dictionary_dirty = True


    def add( self, key_binding ):
        # TODO: Check for duplication?
        self.key_bindings.append( key_binding )
        self.is_dictionary_dirty = True

        
    def build_dictionary( self ):
        print( "Rebuilding key binding dictionary." )
        self.dictionary = {}
        for key_binding in self.key_bindings:
            for key_name in key_binding.key_names:
                self.dictionary[ key_name ] = key_binding

        self.is_dictionary_dirty = False


    def find( self, key_name ):
        if self.is_dictionary_dirty:
            self.build_dictionary()

        return self.dictionary.get( key_name, None )


    def callback( self, key_name ):
        if self.is_dictionary_dirty:
            self.build_dictionary()
            
        key_binding = self.dictionary.get( key_name, None )
        if key_binding is not None:
            key_binding.callback()
        else:
            pass
