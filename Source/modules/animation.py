#
# animation.py
#
# TODO: Add a current_time lambda function?
import time

AUTO_START = 0

class AnimationManager:
    animations = None

    
    def __init__( self ):
        self.animations = []


    def add_animation( self, animation, start_time ):
        self.animations.append( animation )

        if ( start_time == AUTO_START ):
            animation.start()
        else:
            animation.start_time = int( round( time.time() * 1000 ) ) + start_time


    def next_iteration( self ):
        current_time = int( round( time.time() * 1000 ) )
        completed_count = 0
        if len( self.animations ) > 0:
            for animation in self.animations:
                if ( not animation.is_running ) and ( current_time >= animation.start_time ):
                    animation.start()
                
                if animation.is_running:
                    animation.next_iteration()

                    if animation.is_finished:
                        completed_count += 1
                    else:
                        pass
                else:
                    pass

            # Remove completed animations from our list.
            anim_index = 0
            while completed_count > 0:
                animation = self.animations[ anim_index ]
                if animation.is_finished:
                    self.animations.pop( anim_index )
                    completed_count -= 1
                    #print( "Removing completed animation." )
                else:
                    anim_index += 1
            
        else:
            pass


class Animation:
    # All times are in ms.
    start_time = 0
    end_time = 0
    duration = 0
    interval = 0
    last_iteration_time = 0
    progress = 0.0
    is_running = False
    is_finished = False
    callback = None

    
    def __init__( self, interval, duration, callback ):
        self.interval = interval
        self.duration = duration
        self.callback = callback


    def start( self ):
        self.start_time = int( round( time.time() * 1000 ) )
        self.is_running = True
        self.is_finished = False


    def stop( self ):
        # Derived classes can clean up resources in their stop() method.
        self.is_running = False
        self.is_finished = True


    def next_iteration( self ):
        if self.is_running:
            self.last_iteration_time = int( round( time.time() * 1000 ) )
            self.progress = ( 1.0 * self.last_iteration_time - self.start_time ) / self.duration
            
            if self.progress > 1.0:
                self.progress = 1.0
            else:
                pass

            if self.callback is not None:
                self.callback( self )
            else:
                pass

            if self.progress >= 1.0:
                self.stop()
            else:
                pass
        else:
            pass
        
