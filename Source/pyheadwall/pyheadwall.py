#
# pyheadwall.py
#
import re
import os
import sys
import __main__ as main

import gettext

DEF_MODULE_PATH = "modules"
DEF_STATIC_DATA_PATH = "data"
DEF_VAR_DATA_PATH = "var"

MODULE_PATHS = [ \
        "/usr/local/lib/games", \
        "/usr/lib/games" \
        ]

STATIC_DATA_PATHS = [ \
        "/usr/local/share/games", \
        "/usr/share/games" \
        ]

VAR_DATA_PATHS = [ \
        "/usr/local/var/games", \
        "/var/games" \
        ]

PROJECT_TYPE_UNKNOWN = 0
PROJECT_TYPE_APP = 1
PROJECT_TYPE_GAME = 2

class HeadwallProject:
    start_path = ""
    code_path = ""
    static_data_path = ""
    var_data_path = ""
    project_name = "Unnamed Project"
    project_name_safe = "unnamed_project"
    project_type = PROJECT_TYPE_UNKNOWN
    transation = None
    description_brief = ""
    

    def __init__( self, project_name, project_type, brief_description ):
        print( "Init headwall project." )
        self.project_type = project_type
        self.set_project_name( project_name )
        self.description_brief = brief_description
        self.initialise_paths()


    def set_project_name( self, name ):
        self.project_name = name
        self.project_name_safe = re.sub( " ", "_", re.sub( "[^a-z ]", "", name.lower() ).strip() )
        print( "Project: ", self.project_name )
        print( "Safe Name: ", self.project_name_safe )
        

    def initialise_paths( self ):
        self.start_path = os.path.dirname( os.path.abspath( main.__file__ ) )

        # Code modules.
        self.code_path = os.path.join( self.start_path, DEF_MODULE_PATH )
        if not os.path.isdir( self.code_path ):
            self.code_path = ""
            for path in MODULE_PATHS:
                path = os.path.join( os.path.join( path, self.project_name_safe, DEF_MODULE_PATH ) )
                if os.path.isdir( path ):
                    self.code_path = path
                    break
                else:
                    pass
        else:
            pass
        
        # Static shared data.
        self.static_data_path = os.path.join( self.start_path, DEF_STATIC_DATA_PATH )
        if not os.path.isdir( self.static_data_path ):
            self.static_data_path = ""
            for path in STATIC_DATA_PATHS:
                path = os.path.join( os.path.join( path, self.project_name_safe, DEF_STATIC_DATA_PATH ) )
                if os.path.isdir( path ):
                    self.static_data_path = path
                    break
                else:
                    pass
        else:
            pass


        # Variable shared data.
        self.var_data_path = os.path.join( self.start_path, DEF_VAR_DATA_PATH )
        if not os.path.isdir( self.var_data_path ):
            self.var_data_path = ""
            for path in VAR_DATA_PATHS:
                path = os.path.join( os.path.join( path, self.project_name_safe, DEF_VAR_DATA_PATH ) )
                if os.path.isdir( path ):
                    self.var_data_path = path
                    break
                else:
                    pass
        else:
            pass


        # If we have a valid modules path then add it to the list of seach
        # paths.
        if os.path.isdir( self.code_path ):
            sys.path.insert( 0, self.code_path )
        else:
            print( "Failed to establish code path." )

        print( "Start Path: ", self.start_path )
        print( "Code Modules: ", self.code_path )
        print( "Static Data Files: ", self.static_data_path )
        print( "Variable Data Files: ", self.var_data_path )

        self.translation = gettext.translation( \
            self.project_name_safe, \
                self.get_data_file_name( "locale" ), \
                fallback = True \
                )
        global _
        _ = self.translation.gettext
        

    def get_data_file_name( self, file_name ):
        return os.path.join( self.static_data_path, file_name )
