#!/usr/bin/env python3
#
# headtris
#
# A Python 3 tetris clone. The goal is to have a clean and simple tetris clone.
#
# paul@fase.tech
#
from pyheadwall import pyheadwall

PROJECT_NAME = "Headtris"
PROJECT_SHORT_DESC = "A simple and addictive Tetris clone"
TETROMINO_TEMPLATE_FILE_NAME = "tetrominoes.txt"

#
# The project objects manages things like paths across different operating
# systems.
#
my_project = pyheadwall.HeadwallProject( \
    PROJECT_NAME, \
        pyheadwall.PROJECT_TYPE_GAME, \
        PROJECT_SHORT_DESC \
        )

#
# If the project has been established properly then we should be able to import
# our project-specific modules here.
#
import headtris_game
import headtris_window

# Create the game back-end and load the terominoes templates.
game = headtris_game.Game()
game.load_tetrominoes( my_project.get_data_file_name( TETROMINO_TEMPLATE_FILE_NAME ) )

# Create the game window and attach our game to it.
my_window = headtris_window.HeadtrisWindow( my_project )
my_window.set_game( game )
my_window.start()
